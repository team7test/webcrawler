/**
 * This class will get the value of each pivot and display it. Here, pivot is the web page.
 */
package web;

import java.io.IOException;
import java.util.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Engine {

	private List<Pivot> pivotList;

	public Engine(List<Pivot> pivotList) {
		this.pivotList = pivotList;
	}

	/**
	 * Method to search the links and store to the url variable
	 * 
	 * @return
	 */
	private List<Pivot> searchSubPivot() {
		List<Pivot> urls = new ArrayList<>();

		for (Pivot p : this.pivotList) {
			Document doc;
			try {

				// need http protocol
				doc = Jsoup.connect(p.getPivot()).get();

				// get all links
				Elements links = doc.select("a[href]");
				for (Element link : links) {
					// get the value from href attribute
					// System.out.println(link.attr("href"));// the pages with this href property
					urls.add(new Pivot(link.attr("href"))); // adding to url list declared above
				}

			} catch (IOException e) {
				e.printStackTrace();

			}

		}
		return urls;
	}

	/**
	 * For the search of the contents of each linked page
	 * 
	 * @return
	 */
	public List<PageContent> searchSubPivotContent() {

		List<PageContent> pages = new ArrayList<>(); // new variable

		List<Pivot> pivots = this.searchSubPivot();

		for (Pivot p : pivots) {
			Document doc;
			try {

				// need http protocol
				doc = Jsoup.connect(p.getPivot()).get();
				String title = doc.title(); // retrieved pages title section
				String content = doc.body().text(); // retrieved pages content section
				pages.add(new PageContent(p.getPivot(), title, content));
			} catch (IllegalArgumentException e) {
				// ignore
			}

			catch (IOException e) {
				e.printStackTrace();

			}
		}
		return pages;

	}

	public List<Pivot> getPivotList() {
		return pivotList;
	}

	public void setPivotList(List<Pivot> pivotList) {
		this.pivotList = pivotList;
	}

}
