package web;

public class Pivot {

	private String pivot;

	public Pivot(String pivot) {
		this.pivot = pivot;
	}

	// getter and setter using Eclipse.
	// getting and initializing the value
	public String getPivot() {
		return pivot;
	}

	public void setPivot(String pivot) {
		this.pivot = pivot;
	}

}
