/**
 * Here in the program, pivot means the web pages.
 * un-comment the prints to check title, link and content of the pages
 */
package web;

import java.util.ArrayList;
import java.util.List;

public class MainApp {

	public static void main(String[] args) {
		List<Pivot> pivots = new ArrayList<>();

		pivots.add(new Pivot("https://www.google.com/")); // link here

		Engine engine = new Engine(pivots);

		List<PageContent> pages = engine.searchSubPivotContent();
		for (PageContent pg : pages) {
			// System.out.println(pg.getTitle());
			// System.out.println("************************************");
			System.out.println(pg.getLink());
			// System.out.println(pg.getContent());
			// System.out.println("************************************");
		}
	}

}
