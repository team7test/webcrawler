/**
 * Class getting and storing the web content.
 */
package web;

public class PageContent {
	private String link;
	private String title;
	private String content;

	// Values initialization
	public PageContent(String link, String title, String content) {

		this.link = link;
		this.title = title;
		this.content = content;
	}

	public PageContent() {
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
